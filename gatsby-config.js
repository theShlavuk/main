require("dotenv").config()

module.exports = {
    siteMetadata: {
        siteUrl: `http://aegist.esiweb.pro`,
        title: `Aegist`,
        description: `Aegist`,
        author: `@mediakod`,
    },
    plugins: [{
            resolve: "gatsby-plugin-react-axe",
            options: {
                // Integrate react-axe in production. This defaults to false.
                showInProduction: false,

                // Options to pass to axe-core.
                // See: https://github.com/dequelabs/axe-core/blob/master/doc/API.md#api-name-axeconfigure
                axeOptions: {
                    // Your axe-core options.
                },
            },
        },
        {
            resolve: "gatsby-source-wordpress",
            options: {
                baseUrl: process.env.SOURCE_URL,
                protocol: "https",
                hostingWPCOM: false,
                useACF: true,
                acfOptionPageIds: [],
                verboseOutput: false,
                perPage: 100,
                searchAndReplaceContentUrls: {
                    sourceUrl: process.env.SOURCE_URL,
                    replacementUrl: process.env.SITE_URL,
                },
                concurrentRequests: 10,
                includedRoutes: [
                    "**/categories",
                    "**/posts",
                    "**/pages",
                    "**/media",
                    "**/tags",
                    "**/taxonomies",
                    "**/users",
                    "**/*/menus",
                    "**/seo",
                    "**/*/*/menu-locations",
                ],
                excludedRoutes: [],
                normalizer: function({ entities }) {
                    return entities
                },
            },
        },
        `gatsby-plugin-react-helmet`,
        `gatsby-plugin-sitemap`,
        {
            resolve: `gatsby-plugin-robots-txt`,
            options: {
                host: `http://aegist.esiweb.pro`,
                sitemap: `http://aegist.esiweb.pro/sitemap.xml`,
                env: {
                    development: {
                        policy: [{ userAgent: "*", allow: ["/"] }],
                    },
                    production: {
                        policy: [{ userAgent: "*", allow: "/" }],
                    },
                },
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/assets/img`,
            },
        },
        `gatsby-plugin-sass`,
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `Aegist`,
                short_name: `aegist`,
                start_url: `/`,
                background_color: `#302b30`,
                theme_color: `#302b30`,
                display: `minimal-ui`,
                icon: `src/assets/img/favicon.png`,
            },
        },
        `gatsby-plugin-offline`,
        {
            resolve: `gatsby-plugin-netlify`,
            options: {
                headers: {}, // option to add more headers. `Link` headers are transformed by the below criteria
                allPageHeaders: [], // option to add headers for all pages. `Link` headers are transformed by the below criteria
                mergeSecurityHeaders: true, // boolean to turn off the default security headers
                mergeLinkHeaders: true, // boolean to turn off the default gatsby js headers
                mergeCachingHeaders: true, // boolean to turn off the default caching headers
                transformHeaders: (headers, path) => headers, // optional transform for manipulating headers under each path (e.g.sorting), etc.
                generateMatchPathRewrites: true, // boolean to turn off automatic creation of redirect rules for client only paths
            },
        },
    ],
}