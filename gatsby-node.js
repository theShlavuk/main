const path = require(`path`)

const { createFilePath } = require(`gatsby-source-filesystem`)

// using Gatsby Type Builder API
exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  const typeDefs = `
      type wordpress__menus_menus_items implements Node @infer {
         items: [items]
         testimportant: String!
      }

      type items implements Node @infer {
         url: String
      }
   `
  createTypes(typeDefs)
}

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  // templates
  const templatePage = path.resolve("./src/templates/Page.js")
  const templatePageMetier = path.resolve("./src/templates/PageMetier.js")
  const templatePageContact = path.resolve("./src/templates/PageContact.js")

  // source
  return graphql(`
    {
      allWordpressPage {
        edges {
          node {
            wordpress_id
            slug
            template
          }
        }
      }
    }
  `).then(result => {
    if (result.errors) throw result.errors

    const Pages = result.data.allWordpressPage.edges

    Pages.forEach(page => {
      if (page.node.template == "page-metier.php") {
        createPage({
          path: `/${page.node.slug}`,
          component: templatePageMetier,
          context: {
            id: page.node.wordpress_id,
          },
        })
      } else if (page.node.template == "page-contact.php") {
        createPage({
          path: `/${page.node.slug}`,
          component: templatePageContact,
          context: {
            id: page.node.wordpress_id,
          },
        })
      } else {
        createPage({
          path: `/${page.node.slug}`,
          component: templatePage,
          context: {
            id: page.node.wordpress_id,
          },
        })
      }

      // if(page.node.template === 'page-restaurant.php') {
      //    createPage({
      //       path: `/${page.node.slug}`,
      //       component: templatePageRestaurant,
      //       context: {
      //          id: page.node.wordpress_id,
      //       },
      //    })
      // } else if(page.node.template === 'page-traiteur.php') {
      //   createPage({
      //      path: `/${page.node.slug}`,
      //      component: templatePageTraiteur,
      //      context: {
      //         id: page.node.wordpress_id,
      //      },
      //   })
      // } else if(page.node.template === 'page-contact.php') {
      //   createPage({
      //      path: `/${page.node.slug}`,
      //      component: templatePageContact,
      //      context: {
      //         id: page.node.wordpress_id,
      //      },
      //   })
      // } else if(page.node.template === 'page-homepage.php') {
      //   createPage({
      //      path: `${page.node.path}`,
      //      component: templatePageCategory,
      //      context: {
      //         id: page.node.wordpress_id,
      //      },
      //   })
      // } else {

      // }
    })
  })
}
