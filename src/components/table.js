import React from "react"
//import { Link } from "gatsby"

const Table = ({header, heading, data}) => (
  <>
    <div className="process-diagram--heading">
      <h2>{header.left}</h2>
      <h2>{header.right}</h2>
    </div>

    <section className="process-diagram container">
      <h2>{header.left}</h2>
      <div>
        <ul>
          {heading.map((item, index) => <li key={`tableHeading-${index}`}><span>{item.heading}</span></li> )}
        </ul>
      </div>

      <h2>{header.right}</h2>
      {data.map((item, index) => (
        <div key={`tableWork-${index}`}>
          <h2>{item.title}</h2>
          <ul>
            {item.values.map((value, key) => <li key={`tableWorkValue-${key}-${index}`}>{value.texte}</li> )}
          </ul>
        </div>
      ))}
    </section>
  </>
)

export default Table