import React, { useState } from "react"

const Toggle = ({ onToggleUpdate }) => {
    const [ariaPressed, setAriaPressed] = useState(false);

    const handleClick = () => {
        let oldState = ariaPressed

        // On mets à jour l'état
        setAriaPressed(!oldState)

        // On passe l'état dans une fonction
        // pour le passer au parent
        onToggleUpdate(!oldState)
    }
    
    return (
        <button
            aria-label="Menu"
            aria-controls="main-menu"
            aria-pressed={ariaPressed}
            onClick={ () => handleClick() }
        >
            <i></i>
            Menu
        </button>
    )
}

export default Toggle
