import React from "react"

import Img from "../helpers/img"

const Banner = ({ title, description, imgID, isSmall }) => (
  <section className={`banner ${isSmall ? "banner--smaller" : ""}`}>
    <div className="banner__block">
      <h1>{title}</h1>
      {description && <div dangerouslySetInnerHTML={{ __html: description }} />}
    </div>

    <Img imageID={imgID} mSize={`large`} />
  </section>
)

export default Banner
