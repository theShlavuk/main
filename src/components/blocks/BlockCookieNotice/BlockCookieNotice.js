import React from 'react'
import { CookieBanner } from '@palmabit/react-cookie-law';
import './BlockCookieNotice.scss'

function CookieNotice() {
   return (
      <div
         id="cookie-container"
         className={`show-cookie`}
      >
         <CookieBanner
            message={"Ce site utilise des cookies"}
            policyLink={`/politique-de-confidentialite`}
            privacyPolicyLinkText={`En savoir plus`}
            showDeclineButton={true}
            declineButtonText={`Refuser`}
            acceptButtonText={`Accepter`}
            necessaryOptionText={`Nécessaires`}
            preferencesOptionText={`Fonctionnels`}
            statisticsOptionText={`Statistiques`}
            showMarketingOption={false}
            showPreferencesOption={false}

            // Remove default style
            styles={{
               dialog: {},
               container: {},	
               message: {},
               policy: {},
               selectPane: {},
               optionWrapper: {},
               optionLabel: {},
               checkbox: {},
               buttonWrapper: {},
               button: {}
            }}
         />
      </div>
   )
}

export default CookieNotice