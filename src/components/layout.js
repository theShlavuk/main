import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql, Link } from "gatsby"
import Cookies from 'js-cookie'
import ReactGA from 'react-ga'

// Components
import Header from "./header"
import CookieNotice from './blocks/BlockCookieNotice/BlockCookieNotice'

// Helpers
import { removeDomaine } from "../helpers/url"
const Layout = ({ children }) => {
  // Check cookie
  if(Cookies.get('rcl_statistics_consent') === 'true') {
    ReactGA.initialize('UA-415573-7')
  }

  const data = useStaticQuery(graphql`
    {
      wordpressMenusMenusItems(slug: {eq: "footer"}) {
        items {
          slug
          target
          title
          url
        }
      }
    }
  `)

  return (
    <div className="container">
      <a className="screen-reader-text smoothscroll" href="#main" title="Passer au contenu principal">Passer au contenu principal</a>
      
      <Header siteTitle={`Site title`} />
      
      {children}

      <footer>
        <p>
          {data.wordpressMenusMenusItems.items.map((item, index) => {
            return (<Link to={removeDomaine(item.url)} key={`navFooter${index}`} className="nav-footer">{item.title}</Link>)
          })}
          &nbsp;<span>- </span>Réalisé par <a href="https://mediakod.com" rel="noreferrer">Mediakod</a>
        </p>
      </footer>
        
      {!Cookies.get('rcl_consent_given') && <CookieNotice />}
      
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
