import React from 'react'
import { Link } from 'gatsby'

// Helpers
import Img from '../../helpers/img'

//import ImgCpts from '../../assets/img/cpts.jpg'

const CardLink = ({surtitle, title, link, imgID}) => {
   return (
      <Link to={link} className="card-link">
         <h3 className="metier__carttitle">
            <span className="metier__cartspan">{surtitle}</span>
            {title}
         </h3>
         <Img imageID={imgID} mSize={`small`} className="metier__img" />

         {/* <img className="metier__img" src={ImgCpts} alt="Compétence"/> */}
      </Link>
   )
}

export default CardLink