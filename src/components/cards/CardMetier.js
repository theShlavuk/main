import React from 'react'
import Img from '../../helpers/img'

const CardMetier = ({title, iconID, backgroundID}) => (
   <li className="blocks__el">
      <h2 className="blocks__title">{title}</h2>
      {/* <img className="blocks__imgbg" src="https://source.unsplash.com/random/268x395" alt="Fond icon"/> */}

      <Img imageID={backgroundID} mSize={`small`} />

      <div className="blocks__icon">
         <Img imageID={iconID} mSize={`small`} />
      </div>
   </li>
)

export default CardMetier