import { StaticQuery, Link, graphql } from "gatsby"
import React, { useState, useRef } from "react"
// import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';

// Components
import Toggle from "../components/toggle"
import siteLogo from "../assets/img/aegist-logo.svg"
import "../assets/scss/styles.scss"

// Helpers
import { removeDomaine, getDomaine } from "../helpers/url"

const Header = ({ siteTitle }) => {
  const [navbarState, setNavbarState] = useState(false)
  const myFirstLink = useRef(null)
  // let appBody = useRef(document.body)

  function handleToggleClick(ariaPressedState) {
    // Au changement du togge,
    // on recupère son état,
    // et on l'assigne à l'état de la navbar
    setNavbarState(ariaPressedState)

    if (ariaPressedState) {
      //disableBodyScroll(appBody)
      myFirstLink.current.focus()
    } else {
      //enableBodyScroll(appBody)
    }
  }

  return (
    <StaticQuery
      query={graphql`
        {
          wordpressMenusMenusItems(slug: { eq: "main" }) {
            items {
              slug
              target
              title
              url
            }
          }
        }
      `}
      render={data => (
        <header className="site-header">
          <nav
            id="main-menu"
            className={`navbar ${navbarState ? "navbar--active" : ""}`}
          >
            <Link id="brand" to="/">
              <img src={siteLogo} alt={siteTitle} />
            </Link>
            <ul>
              {data.wordpressMenusMenusItems.items.map((item, index) => {
                return index === 0 ? (
                  <li key={`NavItem-${index}`}>
                    {getDomaine(item.url) === "bit.ly" ? (
                      <a
                        href="{item.url}"
                        activeClassName="current-page"
                        ref={myFirstLink}
                      >
                        {item.title}
                      </a>
                    ) : (
                      <Link
                        to={removeDomaine(item.url)}
                        activeClassName="current-page"
                        ref={myFirstLink}
                      >
                        {item.title}
                      </Link>
                    )}
                  </li>
                ) : (
                  <li key={`NavItem-${index}`}>
                    {getDomaine(item.url) === "bit.ly" ? (
                      <a
                        href={removeDomaine(item.url)}
                        activeClassName="current-page"
                        target="{item.target}"
                      >
                        {item.title}
                      </a>
                    ) : (
                      <Link
                        to={removeDomaine(item.url)}
                        activeClassName="current-page"
                      >
                        {item.title}
                      </Link>
                    )}
                  </li>
                )
              })}
            </ul>

            <Toggle onToggleUpdate={handleToggleClick} />
          </nav>
        </header>
      )}
    ></StaticQuery>
  )
}

export default Header
