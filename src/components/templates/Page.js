import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"

const Page = () => (
  <Layout>
    <SEO title="Aegist, mentions légales" />
    <section className="container mention">
      <h2>Une équipe de consultants- associés, tous engagés dans le conseil après des responsabilités opérationnelles.</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In at dapibus ante, id tempor enim. Sed eleifend iaculis nibh sit amet posuere. Donec eu quam non ligula suscipit interdum venenatis eget felis. Donec lobortis tempor accumsan.</p>
      <p>Aliquam quis dapibus lacus, at sagittis quam. Suspendisse feugiat ligula ut massa malesuada efficitur. Etiam non tincidunt nisl. Cras at semper nibh, ac mattis diam. Morbi ac venenatis massa. Suspendisse sit amet orci nec turpis dapibus posuere id at libero. Aenean dapibus luctus gravida. Phasellus sit amet aliquam purus.</p>
    </section>
  </Layout>
)

export default Page
