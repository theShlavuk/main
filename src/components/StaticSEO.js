import React from "react"
import Helmet from "react-helmet"

const StaticSEO = ({ title, customBodyClass }) => {

   return (
      <Helmet
         htmlAttributes={{
            lang: 'fr',
         }}
         title={title}
         bodyAttributes={{ class: customBodyClass || '' }}
      />
   )
}

export default StaticSEO