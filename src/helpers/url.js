export const removeDomaine = url => {
  if (url) {
    // eslint-disable-next-line
    return getDomaine(url) === "bit.ly" ? url : url.replace(/^.*\/\/[^\/]+/, "")
  }
}

export const getDomaine = url => {
  if (url) {
    let urlParts = url
      .replace("http://", "")
      .replace("https://", "")
      .split(/[/?#]/)
    return urlParts[0]
  }
}
