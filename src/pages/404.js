import React from "react"

import Layout from "../components/layout"
import StaticSEO from "../components/StaticSEO"

import Banner from '../components/banner'
import { Link } from "gatsby"

const NotFoundPage = () => (
  <Layout>
    <StaticSEO
        title={`Page 404 - Aegist`}
        customBodyClass={`page-404`}
    />

    <Banner
        title="La page n'existe pas"
        imgID={60}
        isSmall={true}
    />

    <section className="container mention">
      <p>Oups, il semblerait que vous ne soyez pas sur la bonne page.<br /><Link to="/">Retourner sur l'accueil</Link></p> 
    </section>
    
  </Layout>
)

export default NotFoundPage
