import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"

// Component
import Banner from '../components/banner'
import CardLink from '../components/cards/CardLink'

// Helpers
import { removeDomaine } from '../helpers/url'

const IndexPage = ({ data }) => {

  let homepage = data.wordpressPage;

  return (
    <Layout>
      <SEO WPPageID={homepage.wordpress_id} customBodyClass='page-frontpage' />

      <Banner 
        title={homepage.title}
        description={homepage.acf.homepage_loud_subtitle}
        imgID={homepage.acf.homepage_loud_image.wordpress_id}
      />
      
      <section className="video_wrapper" id="homepage_video">
        <div className="video__div">
          <h2 className="video_title title">
            {homepage.acf.homepage_titre_video}
          </h2>
          <div className="video_embed" dangerouslySetInnerHTML={{__html: homepage.acf.homepage_contenu_video}}/>
        </div>
      </section>
      
      <main className="metier" id="main">
        <div className="metier__div">
          <h2 className="title">
            <span className="title-span">{homepage.acf.homepage_work_surtitle}</span>
            {homepage.acf.homepage_work_title}
          </h2>
          <div dangerouslySetInnerHTML={{__html: homepage.acf.homepage_work_text}} />
        </div>
        
        <div className="metier__div card-link--grid">
          {homepage.acf.homepage_work_cards.map((card, index) => (
            <CardLink
              key={`CardLink-${index}`}
              surtitle={card.surtitle}
              title={card.title}
              link={removeDomaine(card.link.url)}
              imgID={card.image.wordpress_id}
            />
          ))}
        </div>
      </main>

      <section className="clients" id="clients">
        <div className="clients__div">
          <h2 className="title">{homepage.acf.homepage_clients_title}</h2>
          <div dangerouslySetInnerHTML={{__html: homepage.acf.homepage_clients_text}} />
        </div>

         <ul className="act">
            {homepage.acf.homepage_clients_sectors.map((client, index) => (
               <li className="act__el" key={`clientSectors-${index}`}>
                  <h3 className="act__title">{client.title}</h3>
                  <p dangerouslySetInnerHTML={{__html: client.text}} />
               </li>
            ))}
        </ul>

        <div className="clients__div clients__div--part">
          <h3>{homepage.acf.homepage_partners_title}</h3>
          <div dangerouslySetInnerHTML={{__html: homepage.acf.homepage_partners_text}} />
        </div>
      </section>
    </Layout>
  )
}

export default IndexPage

export const query = graphql`
  query {
    wordpressPage(template: {eq: "page-homepage.php"}) {
      title
      wordpress_id
      acf {
        homepage_loud_subtitle
        homepage_loud_image {
          wordpress_id
        }
        homepage_work_surtitle
        homepage_work_title
        homepage_work_text
        homepage_work_cards {
          surtitle
          title
          link {
            url
          }
          image {
            wordpress_id
          }
        }
        homepage_clients_title
        homepage_clients_text
        homepage_clients_sectors {
          title
          text
        }
        homepage_partners_title
        homepage_partners_text
      }
    }
  }
`
