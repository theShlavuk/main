import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Img from '../helpers/img'
import Banner from "../components/banner"

const PageContact = ({ data }) => {

  let pageContact = data.wordpressPage

  return (
    <Layout>
      <SEO
        WPPageID={pageContact.wordpress_id}
        customBodyClass="page-contact"
      />
  
      <Banner
        title={pageContact.title}
        imgID={pageContact.acf.contact_loud_image.wordpress_id}
      />

      <main id="main">
        <div className="post-content" dangerouslySetInnerHTML={{__html: pageContact.content}} />
    
        <section id="contact">
            <ul className="contact">
              
              {pageContact.acf.contact_cards.map((card, index) => (
                <li className="contact__el" key={`locationCard-${index}`}>
                  <div className="contact__info">
                    <h2 className="contact__city">{card.title}</h2>
                    <div dangerouslySetInnerHTML={{__html: card.text}} />
                  </div>

                  <Img imageID={card.image.wordpress_id} mSize={`medium`} />
                </li>
              ))}
            </ul>
          </section>

          <section id="team">
            <div className="team">
              <div className="team__text">
                <h2 className="title">
                  <span className="title-span">{pageContact.acf.contact_about_surtitle}</span>
                  {pageContact.acf.contact_about_title}
                </h2>
              </div>


              <ul className="team__list">
                <li className="team__el" dangerouslySetInnerHTML={{__html: pageContact.acf.contact_about_text}}>
                </li>

                {pageContact.acf.contact_about_members.map((team, index) => (
                  <li className="team__el" key={`teamCard-${index}`}>

                    {team.image && <Img imageID={team.image.wordpress_id} mSize={`small`} />}
                    
                    <div>
                      <h3 className="team__title">{team.title}</h3>
                      {(team.tel && team.tel_link) && <a href={`tel:${team.tel_link}`}>{team.tel}</a>}
                      {team.email && <a href={`mailto:${team.email}`}>{team.email}</a>}
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </section>
        </main>
    </Layout>
  )
}

export default PageContact

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: {eq: $id}) {
      title
      content
      wordpress_id
      acf {
        contact_loud_image {
          wordpress_id
        }
        contact_cards {
          title
          text
          image {
            wordpress_id
          }
        }
        contact_about_surtitle
        contact_about_title
        contact_about_text
        contact_about_members {
          image {
            wordpress_id
          }
          title
          tel
          tel_link
          email
        }
      }
    }
  }
`