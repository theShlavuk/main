import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Table from "../components/table"
import Banner from "../components/banner"
import CardMetier from "../components/cards/CardMetier"


const PageMetier = ({ data }) => {

  const pageMetier = data.wordpressPage

  let tableData = [
    {
      title: pageMetier.acf.work_table_column_1_title,
      values: pageMetier.acf.work_table_column_1_values
    },
    {
      title: pageMetier.acf.work_table_column_2_title,
      values: pageMetier.acf.work_table_column_2_values
    },
    {
      title: pageMetier.acf.work_table_column_3_title,
      values: pageMetier.acf.work_table_column_3_values
    },
    {
      title: pageMetier.acf.work_table_column_4_title,
      values: pageMetier.acf.work_table_column_4_values
    }
  ]

  let tableHeading = pageMetier.acf.work_table_headings

  return (
    <Layout>
      <SEO
        customBodyClass="page-metier"
        WPPageID={pageMetier.wordpress_id}
      />
  
      <Banner
        title={pageMetier.title}
        imgID={pageMetier.acf.work_loud_image.wordpress_id}
      />
  
      <main id="main">
        <div className="post-content" dangerouslySetInnerHTML={{__html: pageMetier.content}}/>

        <Table
          header={{
            left: pageMetier.acf.work_table_title_left,
            right: pageMetier.acf.work_table_title_right
          }}
          heading={tableHeading}
          data={tableData} />
      </main>
  
      <section>
        <ul className="blocks">
          {pageMetier.acf.work_cards.map((card, index) => (
            <CardMetier
              key={`cardMetier-${index}`}
              title={card.text}
              iconID={card.icon.wordpress_id}
              backgroundID={card.background}
            />
          ))}
        </ul>
      </section>
    </Layout>
  )
}

export default PageMetier

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: {eq: $id}) {
      title
      content
      wordpress_id
      acf {
        work_loud_image {
          wordpress_id
        }
        work_table_headings {
          heading
        }
        work_table_column_1_title
        work_table_column_1_values {
          texte
        }
        work_table_column_2_title
        work_table_column_2_values {
          texte
        }
        work_table_column_3_title
        work_table_column_3_values {
          texte
        }
        work_table_column_4_title
        work_table_column_4_values {
          texte
        }
        work_table_title_left
        work_table_title_right
        work_cards {
          text
          background
          icon {
            wordpress_id
          }
        }
      }
    }
  }
`
