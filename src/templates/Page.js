import React from "react"
//import { Link } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"

// Components
import Banner from '../components/banner'

const Page = ({ data }) => (
  <Layout>
    <SEO customBodyClass={`page-default`} WPPageID={data.wordpressPage.wordpress_id} />
    
    {data.wordpressPage.acf.page_loud_image && (
      <Banner
        isSmall={true}
        imgID={data.wordpressPage.acf.page_loud_image.wordpress_id}
        title={data.wordpressPage.title}
      />
    )}

    <section className="container mention" dangerouslySetInnerHTML={{__html: data.wordpressPage.content }} />
  </Layout>
)

export default Page

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: {eq: $id}) {
      title
      content
      wordpress_id
      acf {
        page_loud_image {
          wordpress_id
        }
      }
    }
  }
`

